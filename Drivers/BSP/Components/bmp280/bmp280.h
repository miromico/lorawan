/*
 * bosch.h
 *
 *  Created on: 02.04.2015
 *      Author: praktikant
 */

#ifndef _BMP280_H_
#define _BMP280_H_

#include <stdint.h>
#include "stm32l1xx_hal.h"

/** Default I2C address */
#define I2C_ADDR_BMP280	0x76

/** Sensor calibration data struct */
typedef struct __attribute__ ((__packed__)) {
  uint16_t dig_T1;
  int16_t dig_T2;
  int16_t dig_T3;
  uint16_t dig_P1;
  int16_t dig_P2;
  int16_t dig_P3;
  int16_t dig_P4;
  int16_t dig_P5;
  int16_t dig_P6;
  int16_t dig_P7;
  int16_t dig_P8;
  int16_t dig_P9;
} BMPCalibrationData_t;

/** BMP280 sensor device handle type */
typedef struct {
  uint8_t i2caddress; 				///< Is 0x76, meaning you need to write 0x76<<1 to it.
  I2C_HandleTypeDef *hi2c; 			// The I2C Handle for STM32 HAL_Driver
  BMPCalibrationData_t calibration; ///< Calibration data
  uint8_t oversample;				///< Oversample configuration
} BMP280_HandleTypedef;

/**
 * @brief  Resets the sensor
 *
 * @return Returns 0 on success, -1 otherwise
 */
void BMP280Reset(BMP280_HandleTypedef * handle);

/**
  * @brief  Tests sensor and reads out configuration values
  *
  * BMP280 can only have I2C-address 0x76 or 0x77 (normally 0x76).
  * This function should be called before doing anything else with the sensor.
  *
  * @param  handle      A pointer to a BMP280_HandleTypedef
  * @param  hi2c        A pointer to the I2C-Handle
  * @param  i2c_address	The I2C address. Depends on the SDO-Pin
  *
  * @return Returns 0 on success, -1 otherwise
  */
int8_t BMP280Init(BMP280_HandleTypedef * handle, I2C_HandleTypeDef * hi2c, uint8_t i2c_address);

/**
  * @brief  Triggers forced measurement with predefined oversample-settings
  *
  * @param  handle 	A pointer to a BMP280_HandleTypedef
  *
  * @return Returns 0 on success, -1 otherwise
  */
int8_t BMP280Measure(BMP280_HandleTypedef * handle);

/**
  * @brief  Triggers single measurement with predefined oversample-settings
  *
  * Triggers a single measurement and waits for completion to read out measurement data.
  * Data is compensated using the sensor calibration data.
  *
  * @param  handle  A pointer to a BMP280_HandleTypedef
  *
  * @retval T       Compensated temperature in 1/100 degrees celsius
  * @retval P       Compensated pressure in Pa as unsigned 32 bit integer in Q24.8 fixed point format
  *
  * @return Returns 0 on success, -1 otherwise
  */
int8_t BMP280SingleMeasure(BMP280_HandleTypedef * handle, int32_t *T, uint32_t *P);

/**
  * @brief  Gets measurement result
  *
  * Read out sensor and compensate measurement data.
  * @note	Result will be undefined, if sensor is not yet ready
  *
  * @param  handle  A pointer to a BMP280_HandleTypedef
  *
  * @retval T       Compensated temperature in 1/100 degrees celsius
  * @retval P       Compensated pressure in Pa as unsigned 32 bit integer in Q24.8 fixed point format
  *
  * @return Returns 0 on success, -1 otherwise
  */
int8_t BMP280GetMeasurement(BMP280_HandleTypedef * handle, int32_t *T, uint32_t *P);

/** @brief  Gets status of the sensor
  *
  * Read out sensor status. Definition of bit values:
  *
  * bit 0:	sensor is busy
  * bit 4:  sensor is copying data internally
  *
  * @param  handle	A pointer to a BMP280_HandleTypedef
  *
  * @return Any combination of status bits on success, -1 on failure
  */
int8_t BMP280Status(BMP280_HandleTypedef * handle);

int8_t BMP280Cyclic(uint32_t parameter, uint32_t * delay);

/**
  * @brief  Configures oversample settings
  * @note   The amount of measurements done is = 2^(param-1)
  * @note   The max. amount of measurements is 16
  * @note   For best accuracy with minimal calculation time, use parameters (2,5)
  * @note   Do not write anything above 7
  * @note   Writing 0 will skip the measurement
  *
  * @param  t_over	Temperature oversample settings
  * @param  p_over	Pressure oversample settings
  *
  * @return Returns 0 on success, -1 otherwise
  */
int8_t BMP280ConfigureOversample(BMP280_HandleTypedef * handle, uint8_t t_over, uint8_t p_over);

#endif
