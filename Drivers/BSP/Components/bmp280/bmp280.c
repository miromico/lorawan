/*
 * bosch.c
 *
 *  Created on: 02.04.2015
 *      Author: praktikant
 */

#include "bmp280.h"
#include <string.h>

/*
 * For normal measurement, define BOSCH_NORMAL 3 and put it in Bosch_configure_oversample
 * However, normal measurement is not implemented, do not use it without rewriting driver.
 */
#define BOSCH_FORCED 1

// Command macros
#define BOSCH_CONFIG_ADDRESS 0xf5 	// Configure filter, standby-time and SPI
#define BOSCH_MEAS 0xf4 			// Control oversample and mode
#define BOSCH_STATUS 0xf3
#define BOSCH_STATUS_BITS 0b00001001

#define BOSCH_MEASURE_START 0xf7 	// Data of ADC: First pressure, then temperature

#define BOSCH_RESET_ADDRESS 0xe0 	// Write 0xB6 to reset BMP180
#define BOSCH_RESET 0xb6;

#define BOSCH_CALIB_START 0x88 		// Goes up to 0xbf
#define BOSCH_BYTE_AMOUNT 24 		// Amount of calibration bytes

#define BOSCH_DEVICE_ADDRESS 0xd0 	// ID is saved here
#define BOSCH_DEVICE_ID 0x58 		// Is always 0x58

// Prototypes
static int8_t BMP280ReadCalib(BMP280_HandleTypedef * handle);
static int8_t BMP280Check(BMP280_HandleTypedef * handle);
static void BMP280Compensate(BMP280_HandleTypedef * handle, int32_t adc_T, int32_t adc_P, int32_t *T, uint32_t *P);

int8_t BMP280Init(BMP280_HandleTypedef * handle, I2C_HandleTypeDef * hi2c, uint8_t i2c_address)
{
  // Checking validity of parameters
  if (!handle && !hi2c) return -1;
  if (i2c_address != 0x76 && i2c_address != 0x77) return -1;
  // Writing handle
  handle->i2caddress = (i2c_address << 1);
  handle->hi2c = hi2c;
  // Check if Bosch responds (correctly)
  if (BMP280Check(handle) < 0) return -1;
  // Get calibration
  if (BMP280ReadCalib(handle) < 0) return -1;
  // Configuring oversample
  return BMP280ConfigureOversample(handle, 0b001, 0b001);
}

static int8_t BMP280Check(BMP280_HandleTypedef * bosch)
{
  uint8_t buf = BOSCH_DEVICE_ADDRESS;
  if (HAL_I2C_Master_Transmit(bosch->hi2c, bosch->i2caddress, &buf, 1, 100) != HAL_OK)
	  return -1;
  if (HAL_I2C_Master_Receive(bosch->hi2c, bosch->i2caddress, &buf, 1, 100) != HAL_OK)
	  return -1;

  if (buf == BOSCH_DEVICE_ID) {
    return 0;
  }
  return -1;
}

void BMP280Reset(BMP280_HandleTypedef * bosch)
 {
   uint8_t buf[2];
   buf[0] = BOSCH_RESET_ADDRESS;
   buf[1] = BOSCH_RESET;
   HAL_I2C_Master_Transmit(bosch->hi2c, bosch->i2caddress, buf, 2, 100);
 }

static int8_t BMP280ReadCalib(BMP280_HandleTypedef * handle)
{
  uint8_t buf;
  buf = BOSCH_CALIB_START;
  if (HAL_I2C_Master_Transmit(handle->hi2c, handle->i2caddress, &buf, 1, 100))
	  return -1;
  if (HAL_I2C_Master_Receive(handle->hi2c, handle->i2caddress,
		  (uint8_t *)(&(handle->calibration)), BOSCH_BYTE_AMOUNT, 100)) {
	  return -1;
  }
  return 0;
}

int8_t BMP280Measure(BMP280_HandleTypedef * handle)
{
  uint8_t buf[] = {BOSCH_MEAS, (handle->oversample | BOSCH_FORCED)};
  if (HAL_I2C_Master_Transmit(handle->hi2c, handle->i2caddress, buf, 2, 100))
	  return -1;
  return 0;
}

int8_t BMP280SingleMeasure(BMP280_HandleTypedef * handle, int32_t *T, uint32_t *P)
{
  uint8_t bosch_stat = 1;
  if (BMP280Measure(handle) < 0) return -1;
  while (bosch_stat) {
    HAL_Delay(1);
    bosch_stat = BMP280Status(handle);
  }
  return BMP280GetMeasurement(handle, T, P);
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of �5123� equals 51.23 DegC.
// t_fine carries fine temperature as global value
// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of �24674867� represents 24674867/256 = 96386.2 Pa = 963.862 hPa
static void BMP280Compensate(BMP280_HandleTypedef * handle, int32_t adc_T, int32_t adc_P, int32_t *T, uint32_t *P)
{
	int32_t tvar1, tvar2, t_fine;

	// temperature compensation
	tvar1 = ((((adc_T >> 3) -
			((int32_t)(handle->calibration.dig_T1) << 1))) * ((int32_t)(handle->calibration.dig_T2))) >> 11;
	tvar2 = (((((adc_T >> 4) -
			((int32_t)(handle->calibration.dig_T1))) * ((adc_T>>4) - ((int32_t)(handle->calibration.dig_T1)))) >> 12) *
			((int32_t)(handle->calibration.dig_T3))) >> 14;
	t_fine = tvar1 + tvar2;
	*T = (t_fine * 5 + 128) >> 8;

	// pressure compensation
	int64_t var1, var2, p;
	var1 = ((int64_t)t_fine) - 128000;
	var2 = var1 * var1 * (int64_t)(handle->calibration.dig_P6);
	var2 = var2 + ((var1 * (int64_t)(handle->calibration.dig_P5)) << 17);
	var2 = var2 + (((int64_t)(handle->calibration.dig_P4)) << 35);
	var1 = ((var1 * var1 * (int64_t)(handle->calibration.dig_P3)) >> 8)
			+ ((var1 * (int64_t)(handle->calibration.dig_P2)) << 12);
	var1 = (((((int64_t) 1) << 47) + var1)) * ((int64_t)(handle->calibration.dig_P1)) >> 33;
	if (var1 == 0) {
		// avoid exception caused by division by zero
		*P = 0;
		return;
	}
	p = 1048576 - adc_P;
	p = (((p << 31) - var2) * 3125) / var1;
	var1 = (((int64_t)(handle->calibration.dig_P9)) * (p >> 13) * (p >> 13)) >> 25;
	var2 = (((int64_t)(handle->calibration.dig_P8)) * p) >> 19;
	*P = (uint32_t)(((p + var1 + var2) >> 8) + (((int64_t)(handle->calibration.dig_P7)) << 4));
}

int8_t BMP280GetMeasurement(BMP280_HandleTypedef * handle, int32_t *T, uint32_t *P)
{
  uint8_t buf[6];
  int32_t adc_T, adc_P;

  buf[0] = BOSCH_MEASURE_START;
  if (HAL_I2C_Master_Transmit(handle->hi2c, handle->i2caddress, buf, 1, 100) != HAL_OK)
	  return -1;
  if (HAL_I2C_Master_Receive(handle->hi2c, handle->i2caddress, buf, 6, 100) != HAL_OK)
	  return -1;

  adc_P = (int32_t)((buf[0] << 12) | (buf[1] << 4) | (buf[2] >> 4));
  adc_T = (int32_t)((buf[3] << 12) | (buf[4] << 4) | (buf[5] >> 4));
  BMP280Compensate(handle, adc_T, adc_P, T, P);
  return 0;
}

int8_t BMP280Status(BMP280_HandleTypedef * bosch)
{
  uint8_t buf = BOSCH_STATUS;
  if (HAL_I2C_Master_Transmit(bosch->hi2c, bosch->i2caddress, &buf, 1, 100)) return -1;
  if (HAL_I2C_Master_Receive(bosch->hi2c, bosch->i2caddress, &buf, 1, 100)) return -1;
  return buf & BOSCH_STATUS_BITS;
}

int8_t BMP280Cyclic(uint32_t parameter, uint32_t * delay)
{
  if (parameter == 0) {
    *delay = 0; //UINT32_MAX;
  } else if (parameter < 80) {
    *delay = parameter;
    return 2; // Warning: Measurement period may be too short
  } else {
    *delay = parameter;
  }
  return 0;
}

int8_t BMP280ConfigureOversample(BMP280_HandleTypedef * handle, uint8_t t_over, uint8_t p_over)
{
  if (t_over > 7 || p_over > 7) {
    return -1; // Invalid instruction error
  }
  handle->oversample = (t_over << 5) | (p_over << 2);
  return 0;
}
