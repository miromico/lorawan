/**
  ******************************************************************************
  * @file    FMLR.c
  * @author  Alex Raimondi
  * @version V1.0.0
  * @date    17-Dec-2016
  * @brief   This file contains definitions for:
  *          - LEDs available on FMLR
  *          - Flash device on FMLR
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FMLR.h"

/** @addtogroup BSP
  * @{
  */

/** @defgroup MIROMICO_FMLR MIROMICO_FMLR
  * @brief This file provides set of firmware functions to manage Leds and push-button
  *        available on STM32L1XX-Nucleo Kit from STMicroelectronics.
  *        It provides also LCD, joystick and uSD functions to communicate with
  *        Adafruit 1.8" TFT LCD shield (reference ID 802)
  * @{
  */


/** @defgroup MIROMICO_FMLR_Private_Defines Private Defines
  * @{
  */

/**
* @brief STM32L152RE NUCLEO BSP Driver version
*/
#define __MIROMICO_FMLR_BSP_VERSION_MAIN   (0x01) /*!< [31:24] main version */
#define __MIROMICO_FMLR_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __MIROMICO_FMLR_BSP_VERSION_SUB2   (0x00) /*!< [15:8]  sub2 version */
#define __MIROMICO_FMLR_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */
#define __MIROMICO_FMLR_BSP_VERSION       ((__MIROMICO_FMLR_BSP_VERSION_MAIN << 24)\
                                             |(__MIROMICO_FMLR_BSP_VERSION_SUB1 << 16)\
                                             |(__MIROMICO_FMLR_BSP_VERSION_SUB2 << 8 )\
                                             |(__MIROMICO_FMLR_BSP_VERSION_RC))

/**
  * @}
  */


/** @defgroup MIROMICO_FMLR_Private_Variables Private Variables
  * @{
  */
GPIO_TypeDef* GPIO_PORT[LEDn] = {LEDR_GPIO_PORT, LEDG_GPIO_PORT};

const uint16_t GPIO_PIN[LEDn] = {LEDR_PIN, LEDG_PIN};

/**
  * @}
  */

/** @defgroup MIROMICO_FMLR_Exported_Functions Exported Functions
  * @{
  */

/**
  * @brief  This method returns the STM32L1XX NUCLEO BSP Driver revision
  * @retval version : 0xXYZR (8bits for each decimal, R for RC)
  */
uint32_t BSP_GetVersion(void)
{
  return __MIROMICO_FMLR_BSP_VERSION;
}

/** @defgroup MIROMICO_FMLR_LED_Functions LED Functions
  * @{
  */

/**
  * @brief  Configures LED GPIO.
  * @param  Led: LED to be configured.
  *          This parameter can be one of the following values:
  *     @arg LED2
  * @retval None
  */
void BSP_LED_Init(Led_TypeDef Led)
{
  GPIO_InitTypeDef  gpioinitstruct = {0};

  /* Enable the GPIO_LED Clock */
  LEDx_GPIO_CLK_ENABLE(Led);

  /* Configure the GPIO_LED pin */
  gpioinitstruct.Pin    = GPIO_PIN[Led];
  gpioinitstruct.Mode   = GPIO_MODE_OUTPUT_PP;
  gpioinitstruct.Pull   = GPIO_PULLUP;
  gpioinitstruct.Speed  = GPIO_SPEED_HIGH;
  HAL_GPIO_Init(GPIO_PORT[Led], &gpioinitstruct);

  /* Reset PIN to switch off the LED */
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on.
  *   This parameter can be one of following parameters:
  *     @arg LED2
  * @retval None
  */
void BSP_LED_On(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_SET);
}

/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off.
  *   This parameter can be one of following parameters:
  *     @arg LED2
  * @retval None
  */
void BSP_LED_Off(Led_TypeDef Led)
{
  HAL_GPIO_WritePin(GPIO_PORT[Led], GPIO_PIN[Led], GPIO_PIN_RESET);
}

/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled.
  *   This parameter can be one of following parameters:
  *            @arg  LED2
  * @retval None
  */
void BSP_LED_Toggle(Led_TypeDef Led)
{
  HAL_GPIO_TogglePin(GPIO_PORT[Led], GPIO_PIN[Led]);
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
